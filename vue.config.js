module.exports = {
  configureWebpack: {
    externals: {
      crypto: 'crypto',
    },
  },
  css: {
    loaderOptions: {
      sass: {
        sassOptions: {
          includePaths: ['./src/assets/styles'],
        },
      },
    },
  },
  pluginOptions: {
    webpackBundleAnalyzer: {
      analyzerMode: process.env.WEBPACK_ANALYZER ? 'static' : 'json',
      generateStatsFile: process.env.NODE_ENV !== 'development',
    },
  },
};

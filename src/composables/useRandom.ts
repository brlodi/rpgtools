import {
  bool,
  dice,
  die,
  integer,
  MersenneTwister19937,
  pick,
} from 'random-js';

export function useRandom() {
  const engine = MersenneTwister19937.autoSeed();

  const randomInt = (min: number, max: number) => integer(min, max)(engine);
  const randomBool = (percentTrue?: number) =>
    (typeof percentTrue !== 'undefined' ? bool(percentTrue) : bool())(engine);
  const pickRandomly = (collection: unknown[]) => pick(engine, collection);
  const rollDie = (sides = 6, modifier = 0) => die(sides)(engine) + modifier;
  const rollDice = (count = 1, sides = 6, modifier = 0) =>
    dice(sides, count)(engine).map(x => x + modifier);
  const rollDiceSum = (count = 1, sides = 6, modifier = 0) =>
    dice(sides, count)(engine).reduce((acc, x) => acc + x) + modifier;

  return {
    randomInt,
    randomBool,
    pickRandomly,
    rollDie,
    rollDice,
    rollDiceSum,
  };
}

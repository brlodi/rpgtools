import { Turn } from './Turn';

export interface Encounter {
  id: string;
  name: string;
  turns: Turn[];
}

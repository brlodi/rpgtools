export interface Turn {
  id: string;
  title: string;
  initiative: number;
  modifier?: number;
  advantage?: boolean;
  plus1d10?: boolean;
}
